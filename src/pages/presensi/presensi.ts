import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { SearcPage }from '../../pages/searc/searc';
import { DetailPresensiPage } from '../../pages/detail-presensi/detail-presensi';
import { DashboardPage} from '../../pages/dashboard/dashboard';



/**
 * Generated class for the PresensiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
 selector: 'page-presensi',
 templateUrl: 'presensi.html',
})
export class PresensiPage {
 	public data : any;
 	data1:any;
 	data2:any;
 	constructor(public navCtrl: NavController, 
 		public navParams: NavParams,
 		public modal:ModalController
 		) {
 	}
 	presentProfileModal() {
   		let profileModal = this.modal.create(SearcPage, { userId: 8675309 });
   		profileModal.onDidDismiss(data => {
     		this.data1=data.data_a;
     		this.data2=data.data_b;
   		});
   		profileModal.present();
   		this.klik();
 	}
 	klik(){
		console.log(this.data1+this.data2);
	}
	detail(){
		this.navCtrl.push(DetailPresensiPage);
	}
	gotoDashboard(){
		this.navCtrl.setRoot(DashboardPage);
	}
	ionViewDidLoad() {
   		console.log('ionViewDidLoad PresensiPage');
	}
}
