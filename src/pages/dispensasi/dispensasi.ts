import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { DetailDispensasiPage } from '../../pages/detail-dispensasi/detail-dispensasi';
/**
 * Generated class for the DispensasiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dispensasi',
  templateUrl: 'dispensasi.html',
})
export class DispensasiPage {
  public data : { nama:any , nip:any , awal:any, ahir:any };
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DispensasiPage');
  }

  detail(){
    this.navCtrl.push(DetailDispensasiPage);
  }

  gotoDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }

}
