import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailPresensiPage } from './detail-presensi';

@NgModule({
  declarations: [
    DetailPresensiPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailPresensiPage),
  ],
})
export class DetailPresensiPageModule {}
