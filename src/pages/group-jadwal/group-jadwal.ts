import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashboardPage} from '../../pages/dashboard/dashboard';
import { JadwalDetailPage }from '../jadwal-detail/jadwal-detail';
/**
 * Generated class for the GroupJadwalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group-jadwal',
  templateUrl: 'group-jadwal.html',
})
export class GroupJadwalPage {
  data :any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.data = "bulanan";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupJadwalPage');
  }
  getDetail(parameter){
    this.navCtrl.push(JadwalDetailPage);
  }
  gotoDashboard(){
    this.navCtrl.setRoot(DashboardPage);
  }

}
