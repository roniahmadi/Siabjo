import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KoneksiFingerprintPage } from './koneksi-fingerprint';

@NgModule({
  declarations: [
    KoneksiFingerprintPage,
  ],
  imports: [
    IonicPageModule.forChild(KoneksiFingerprintPage),
  ],
})
export class KoneksiFingerprintPageModule {}
